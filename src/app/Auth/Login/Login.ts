/**
 * @Author  : Mohit Kumar
 * @Created : 24/09/2018
 */
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Constants } from '../../config/constants';
import { FlashMessagesService } from 'angular2-flash-messages';
import { User } from '../../Models/User';
import { Auth } from '../../Services/Auth';

const helper = new JwtHelperService();            /**call json web token service */
const constants = new Constants;

@Component({
    selector: 'app-login',
    templateUrl: './login.html'
})

/**
 * Login Class To Handle Login Requests
 */
export class Login implements OnInit 
{
    // Declarations
    @ViewChild('userForm') userForm: ElementRef;
    user: User = new User;
    responseMessage = '';

    // constructor
    constructor(
        private authService: Auth,
        private router: Router,
        private flashMessagesService: FlashMessagesService,
        private cookieService: CookieService
    ) { }

    // OnInit
    ngOnInit() { }

    /**
     * @param event 
     * for submit form on enter
     */
    keyDownFunction(event) 
    {
        if (event.keyCode == 13 && this.user.email.length > 0 && this.user.password.length > 0) 
        {
            this.onSubmit();
        }
    }

    /**
     * fuction for submit the login form 
     */
    onSubmit()
    {
        this.authService.login(this.user)
            .subscribe(user =>
            {
                if (user['status'])
                {
                    const decodedToken = helper.decodeToken(user.token);
                    this.cookieService.set('token', user.token);
                    this.cookieService.set('role', decodedToken.user.role);
                    this.cookieService.set('username', decodedToken.user.name);
                    this.cookieService.set('id',decodedToken.user.id);

                        window.location.href = 'admin/slugs'
                   
                }
                else 
                {
                    
                    this.flashMessagesService.show(user['message'], { cssClass: 'alert-danger', timeout: 10000 });
                }
            });
    }

}
