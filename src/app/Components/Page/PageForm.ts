/**
 * @Author  : Mohit Kumar
 * @Created : 24/09/2018
 */
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Constants } from '../../config/constants';
import { FlashMessagesService } from 'angular2-flash-messages';
import { DatePipe } from '@angular/common';
import { CookieService } from 'ngx-cookie-service';
import { Common } from '../../Services/Common';
import { Page } from '../../Models/Page';

const cookieService = new CookieService(document);     /** call cookie service to get role from cookies */
const constants = new Constants;

// Decorators
@Component({
    selector: 'user-form',
    templateUrl: './form.html'
})

/**
 * PageForm Class To Handle Addition And Updation Of Users
 */
export class PageForm implements OnInit
{

    // Declarations
    @ViewChild('userForm') userForm: ElementRef;

    page: Page = new Page();
    username: String;
    urlPath = [];
    action = 'add';
    responseMessage = '';
    slugList: any;
    // Constructor
    constructor(
        private route: ActivatedRoute,
        private flashMessagesService: FlashMessagesService,
        private router: Router,
        private datePipe: DatePipe,
        private commonService: Common,
    ) { }

    // OnInit
    ngOnInit()
    {
        this.urlPath = this.route.url['value'];

        if (this.urlPath[1].path == 'edit') 
        {
            this.getPage();
            this.action = 'edit';
        }
        this.getSlugList();
    }
    /**
     * returns currecnt date time
     */
    currentDate()
    {
        var date = new Date();
        let latest_date = this.datePipe.transform(date, 'yyyy-MM-dd  HH:mm:ss ');
        return latest_date;
    }

    /**
     * Gets single user during updation
     */
    getPage()
    {
        const id = +this.route.snapshot.paramMap.get('id');
        this.commonService.getView('admin/pages', id)
            .subscribe(user => { this.page = user['data']; });
    }

    /**
     *  Adds user
     */
    add(): void 
    {
        this.page.is_active = 1;
        this.page.created_at = this.currentDate();
        this.page.updated_at = this.currentDate();
        this.page.updated_by = Number(cookieService.get('id'));
        this.page.created_by = Number(cookieService.get('id'));

        this.commonService.addRecords('admin/pages', this.page)
            .subscribe(res => 
            {
                if (res['status'] == 1)
                {
                    this.flashMessagesService.show(res['message'], { cssClass: 'alert-success', timeout: 2000 });
                }
                else
                {
                    this.flashMessagesService.show( res['message'], { cssClass: 'alert-danger', timeout: 2000 });
                }

            });

    }

    /**
     * Updates Page
     */
    update(): void
    {
        this.commonService.updateRecords('admin/pages', this.page)
            .subscribe(res => 
            {
                this.router.navigate(["/admin/pages"]);
                this.flashMessagesService.show(res['message'], { cssClass: 'alert-success', timeout: 2000 });
            });
    }

    /**
      * Redirect page to summary on click cancel
      */
    cancel()
    {
        this.router.navigate(["/admin/pages"]);
    }

    /**
    * Gets Page List
    */
    getSlugList() 
    {
        this.commonService.getLists('admin/slugs/list/sluglist')
            .subscribe(response =>
            {
                this.slugList = response['data'];
            });
    }


}
