/**
 * @Author  : Mohit Kumar
 * @Created : 24/09/2018
 */
import { Component, OnInit } from '@angular/core';
import { FlashMessagesService } from 'angular2-flash-messages';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { Pagination } from '../../Models/Pagination';
import { Constants } from '../../config/constants';
import { Common } from '../../Services/Common';
import { Page } from '../../Models/Page';

const cookieService = new CookieService(document);
const constants = new Constants

// Decorators
@Component({
  selector: 'user-summary',
  templateUrl: './summary.html'
})

/**
 * PageSummary Class Handles The Functionality Of Summary
 */
export class PageSummary implements OnInit 
{
  //Declarations
  pagination      = new Pagination;
  pages         =[] 
  searchName  = '';
  sortFieldname   = 'id';
  sortOrder       = 'desc';
  selected        = [];
  searchArray     :any;
  slugAllList :any;
  pageToNavigate = 1;

  // Constructor
  constructor(
    private flashMessagesService: FlashMessagesService,
    public router: Router,
    private commonService : Common
  ) { }

  // OnInit
  ngOnInit()
  {
   
      this.getPages();
      this.getSlugAllList();
  }

  /**
   * Gets Pages summary
   */
  getPages() 
  {
      this.searchArray = [
        { key: 'name', value: this.searchName } ]

      this.commonService.getRecords('admin/pages',this.searchArray, this.sortFieldname, this.sortOrder, this.pageToNavigate)
      .subscribe(res =>
      {
          this.pages = res['data']['data'];
          this.pagination.update( res['data']);
      });
  }
  
  /**
   * @param page 
   * Navigates on different pages
   */
  onPageChange(page: number)
  {
    this.pageToNavigate = page;
      this.getPages();
  }

  /**
   * @param sf 
   *  Sorts the summary
   */
  onSortChange(sf: string)
  {
      let so                 = event.srcElement.className;
      this.sortFieldname     = sf;
      this.sortOrder         = so;
      this.pagination.offset = 0;
      this.getPages();
  }

  /**
   * Search Data
   */
  searchData() 
  {
      this.sortFieldname     = 'id';
      this.sortOrder         = 'desc';
      this.pagination.offset = 0;
      this.getPages();
  }

  /**
   * Clear search
   */
  clearSearch() 
  {
      this.searchName    = '';
      this.sortFieldname     = 'id';
      this.sortOrder         = 'desc';
      this.pagination.offset = 0;
      this.getPages();
  }

    /**
    * Gets Slug List
    */
   getSlugAllList() 
   {
       this.commonService.getLists('admin/slugs/list/slugalllist')
           .subscribe(response =>
           {
               this.slugAllList = response['data'];
           });
   }

}
