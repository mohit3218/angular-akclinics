/**
 * @Author  : Mohit Thakur
 * @Created : 27/09/2018
 */
import { Component, OnInit,TemplateRef } from '@angular/core';
import { FlashMessagesService } from 'angular2-flash-messages';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { Pagination } from '../../Models/Pagination';
import { Constants } from '../../config/constants';
import { Common } from '../../Services/Common';
import { Page } from '../../Models/Page';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

const constants = new Constants

// Decorators
@Component({
  selector: 'user-summary',
  templateUrl: './summary.html'
})

/**
 * ContactSummary Class Handles The Functionality Of Summary
 */
export class ContactSummary implements OnInit 
{
    modalRef: BsModalRef;
  //Declarations
  pagination      = new Pagination;
  contacts         =[] 
  searchName  = '';
  searchType = '';
  sortFieldname   = 'id';
  sortOrder       = 'desc';
  selected        = [];
  searchArray     :any;
  pageToNavigate = 1;
  enquireTypes = constants.ENQUIRE_TYPES;

  // Constructor
  constructor(
    private flashMessagesService: FlashMessagesService,
    public router: Router,
    private commonService : Common,
    private modalService: BsModalService
  ) { }

  // OnInit
  ngOnInit()
  {
   
      this.getContactSummary();
  }

  /**
   * Gets Contact summary
   */
  getContactSummary() 
  {
      this.searchArray = [
        { key: 'name', value: this.searchName },
        { key: 'enquire_types', value: this.searchType } 
      ]

      this.commonService.getRecords('admin/contacts',this.searchArray, this.sortFieldname, this.sortOrder, this.pageToNavigate)
      .subscribe(res =>
      {
          this.contacts = res['data']['data'];
          this.pagination.update( res['data']);
      });
  }
  
  /**
   * @param page 
   * Navigates on different pages
   */
  onPageChange(page: number)
  {
    this.pageToNavigate = page;
      this.getContactSummary();
  }

  /**
   * @param sf 
   *  Sorts the summary
   */
  onSortChange(sf: string)
  {
      let so                 = event.srcElement.className;
      this.sortFieldname     = sf;
      this.sortOrder         = so;
        this.getContactSummary();
    }

    /**
   * Search
   */
    searchData() 
    {
        this.sortFieldname = 'id';
        this.sortOrder = 'desc';
        this.getContactSummary();
    }

    /**
     *  Clear search
     */
    clearSearch() 
    {
        this.searchName = '';
        this.searchType = '';
        this.sortFieldname = 'id';
        this.sortOrder = 'desc';
      this.getContactSummary();
  }
  
   /**
   * @param id 
   * Deletes the record
   */
  delete(id): void
  {
          this.commonService.deleteRecords('admin/contacts', id)
              .subscribe(r => 
              {
                  this.contacts = this.contacts.filter(u => u.id !== id);
                  this.flashMessagesService.show(r['message'], { cssClass: 'alert-success' });
                  this.getContactSummary();
              });
  }
  

  openModal(template: TemplateRef<any>  ) {
    this.modalRef = this.modalService.show(template, {class: 'modal-sm contact-message'});
  }
 
  confirm(id): void {
   this.delete(id);
    this.modalRef.hide();
  }
 
  decline(): void {
    this.modalRef.hide();
  }

  openMessage(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template,{ class: ' contact-message' });
  }
}
