
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Common } from '../../Services/Common';
import { Constants } from '../../config/constants';

const constants = new Constants;

// Decorator
@Component({
    selector: 'contact-frontend',
    templateUrl: './contact-frontend.html',
    styleUrls: ['./contact-frontend.css']
})

/**
 * ContactFrontend Class To Handle Addition And Updation Of Users
 */
export class ContactFrontend implements OnInit
{
    // Declarations
    urlPath = [];
    action = 'add';
    responseMessage = '';
    selectedFile: File;
    contents = [];
    
    // Constructor
    constructor(
        private flashMessagesService: FlashMessagesService,
        private commonService: Common) { }

    // OnInit
    ngOnInit()
    {
        this.commonService.loadingSpinner();
    }
}
