/**
 * @Author  : Mohit Thakur
 * @Created : 27/09/2018
 */
import { Component, OnInit, ViewChild, ElementRef,ViewContainerRef } from '@angular/core';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Contact } from '../../../Models/Contact';
import {ToastaService, ToastaConfig, ToastaComponent, ToastOptions, ToastData} from 'ngx-toasta';
import { Common } from '../../../Services/Common';
import { Constants } from '../../../config/constants';
import { DatePipe } from '@angular/common';
import { CookieService } from 'ngx-cookie-service';

const cookieService = new CookieService('id');     /** call cookie service to get role from cookies */
const constants = new Constants;

// Decorator
@Component({
    selector: 'contact-form',
    templateUrl: './contact-form.html',
})


/**
 * Contact Form Class To Handle Addition And Updation Of Users
 */
export class ContactForm implements OnInit
{

    // Declarations
    @ViewChild('userForm') userForm: ElementRef;

    contact: Contact = new Contact();
    urlPath = [];
    action = 'add';
    responseMessage = '';
    selectedFile: File;
    enquireTypes = constants.ENQUIRE_TYPES;
    // Constructor
    constructor(
        private flashMessagesService: FlashMessagesService,
        private commonService: Common,
        private datePipe: DatePipe,
        private toastaService:ToastaService,
        private toastaConfig: ToastaConfig
    ) {  this.toastaConfig.theme = 'bootstrap';}

    // OnInit
    ngOnInit()
    {
    }

    /**
     * returns currecnt date time
     */
    currentDate()
    {
        var date = new Date();
        let latest_date = this.datePipe.transform(date, 'yyyy-MM-dd  HH:mm:ss ');
        return latest_date;
    }

    /**
     *  Adds Enquiry
     */
    add(): void 
    {
        this.contact.created_at = this.currentDate();
        this.contact.updated_at = this.currentDate();
        this.contact.updated_by = Number(cookieService.get('id'));
        this.contact.created_by = Number(cookieService.get('id'));

        this.commonService.addRecords('contacts', this.contact)
            .subscribe(res => 
            {
                if (res['status'] == 1)
                {
                    console.log(res['message']+"***");
                    let element: HTMLElement = document.getElementsByClassName('reset-form')[0] as HTMLElement;

                    var toastOptions:ToastOptions = {
                        title: "My title",
                        msg: res['message'],
                        showClose: true,
                        timeout: 5000,
                        theme: 'bootstrap',
                        onAdd: (toast:ToastData) => {
                            console.log('Toast ' + toast.id + ' has been added!');
                        },
                        onRemove: function(toast:ToastData) {
                            console.log('Toast ' + toast.id + ' has been removed!');
                        }
                    };

                    this.toastaService.success(toastOptions);
                    element.click();
                }
                else
                {
                    this.toastaService.warning(toastOptions);
                }

            });

    }

}
