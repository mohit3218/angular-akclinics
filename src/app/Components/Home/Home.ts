
import { Component, OnInit, AfterViewInit, AfterContentChecked, AfterContentInit, AfterViewChecked } from '@angular/core';
import { Common } from '../../Services/Common';
import { Router } from '@angular/router';
import { Constants } from '../../config/constants';
import { MetaConfig, MetaService } from 'ng2-meta'; 
import { Meta } from '@angular/platform-browser';
const constants = new Constants;

// Decorator
@Component({
    selector: 'home',
    templateUrl: './home.html',
    styleUrls: ['./home.css']
})

/**
 * Home class
 */
export class Home implements OnInit
{
    record = [];
    searchArray =[];
    searchType = 1;
    sortFieldname = 'id';
    sortOrder = 'desc';
    pageToNavigate = 1;

    // constructor
    constructor(  private commonService: Common, public router: Router, private metaService: MetaService, private meta: Meta)
    {
    }

    //ngOnit
    ngOnInit()
    {
        this.commonService.loadingSpinner();

        //get home page content
        this.commonService.getView('pages/pageRecord', constants.homePageId)
        .subscribe(res =>
        {
            this.record = res['data'];
            this.metaService.setTitle(this.record['meta_title']);
            this.metaService.setTag('og:title',this.record['og_title']);
            this.metaService.setTag('og:description',this.record['og_description']);
            this.metaService.setTag('og:image',this.record['og_image']);
            this.meta.addTag({ name: 'description', content: this.record['meta_description'] });
            this.meta.addTag({ name: 'keywords', content: this.record['meta_keywords'] },true);
        });
    }

}
