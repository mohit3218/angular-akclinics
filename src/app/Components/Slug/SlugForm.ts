/**
 * @Author  : Mohit Kumar
 * @Created : 29/09/2018
 */
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Constants } from '../../config/constants';
import { FlashMessagesService } from 'angular2-flash-messages';
import { DatePipe } from '@angular/common';
import { CookieService } from 'ngx-cookie-service';
import { Common } from '../../Services/Common';
import { Slug } from '../../Models/Slug';

const cookieService = new CookieService(document);     /** call cookie service to get role from cookies */
const constants = new Constants;

// Decorators
@Component({
    selector: 'user-form',
    templateUrl: './form.html'
})

/**
 * PageForm Class To Handle Addition And Updation Of Users
 */
export class SlugForm implements OnInit
{

    // Declarations
    @ViewChild('userForm') userForm: ElementRef;

    slug: Slug = new Slug();
    username: String;
    urlPath = [];
    action = 'add';
    responseMessage = '';
    // Constructor
    constructor(
        private route: ActivatedRoute,
        private flashMessagesService: FlashMessagesService,
        private router: Router,
        private datePipe: DatePipe,
        private commonService: Common,
    ) { }

    // OnInit
    ngOnInit()
    {
        this.urlPath = this.route.url['value'];

        if (this.urlPath[1].path == 'edit') 
        {
            this.getSlug();
            this.action = 'edit';
        }
    }
    /**
     * returns currecnt date time
     */
    currentDate()
    {
        var date = new Date();
        let latest_date = this.datePipe.transform(date, 'yyyy-MM-dd  HH:mm:ss ');
        return latest_date;
    }

    /**
     * Gets single user during updation
     */
    getSlug()
    {
        const id = +this.route.snapshot.paramMap.get('id');
        this.commonService.getView('admin/slugs', id)
            .subscribe(user => { this.slug = user['data']; });
    }

    /**
     *  Adds user
     */
    add(): void 
    {
        this.slug.is_active = 1;
        this.slug.created_at = this.currentDate();
        this.slug.updated_at = this.currentDate();
        this.slug.updated_by = Number(cookieService.get('id'));
        this.slug.created_by = Number(cookieService.get('id'));

        this.commonService.addRecords('admin/slugs', this.slug)
            .subscribe(res => 
            {
                if (res['status'] == 1)
                {
                    this.flashMessagesService.show(res['message'], { cssClass: 'alert-success', timeout: 2000 });
                }
                else
                {
                    this.flashMessagesService.show( res['message'], { cssClass: 'alert-danger', timeout: 2000 });
                }

            });

    }

    /**
     * Updates Page
     */
    update(): void
    {
        this.commonService.updateRecords('admin/slugs', this.slug)
            .subscribe(res => 
            {
                this.router.navigate(["/admin/slugs"]);
                this.flashMessagesService.show(res['message'], { cssClass: 'alert-success', timeout: 2000 });
            });
    }

    /**
      * Redirect page to summary on click cancel
      */
    cancel()
    {
        this.router.navigate(["/admin/slugs"]);
    }

}
