/**
 * @Author  : Mohit Kumar
 * @Created : 24/09/2018
 */
import { Component, OnInit } from '@angular/core';
import { FlashMessagesService } from 'angular2-flash-messages';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { Pagination } from '../../Models/Pagination';
import { Constants } from '../../config/constants';
import { Common } from '../../Services/Common';
import { Slug } from '../../Models/Slug';

const cookieService = new CookieService(document);
const constants = new Constants

// Decorators
@Component({
  selector: 'user-summary',
  templateUrl: './summary.html'
})

/**
 * PageSummary Class Handles The Functionality Of Summary
 */
export class SlugSummary implements OnInit 
{
  //Declarations
  pagination      = new Pagination;
  slugs         =[] 
  searchName  = '';
  sortFieldname   = 'id';
  sortOrder       = 'desc';
  selected        = [];
  searchArray     :any;
  pageToNavigate = 1;

  // Constructor
  constructor(
    private flashMessagesService: FlashMessagesService,
    public router: Router,
    private commonService : Common
  ) { }

  // OnInit
  ngOnInit()
  {
   
      this.getSlugs();
  }

  /**
   * Gets Pages summary
   */
  getSlugs() 
  {
      this.searchArray = [
        { key: 'name', value: this.searchName } ]

      this.commonService.getRecords('admin/slugs',this.searchArray, this.sortFieldname, this.sortOrder, this.pageToNavigate)
      .subscribe(res =>
      {
          this.slugs = res['data']['data'];
          this.pagination.update( res['data']);
      });
  }
  
  /**
   * @param slug 
   * Navigates on different pages
   */
  onPageChange(slug: number)
  {
    this.pageToNavigate = slug;
      this.getSlugs();
  }

  /**
   * @param sf 
   *  Sorts the summary
   */
  onSortChange(sf: string)
  {
      let so                 = event.srcElement.className;
      this.sortFieldname     = sf;
      this.sortOrder         = so;
      this.pagination.offset = 0;
      this.getSlugs();
  }

  /**
   * Search Data
   */
  searchData() 
  {
      this.sortFieldname     = 'id';
      this.sortOrder         = 'desc';
      this.pagination.offset = 0;
      this.getSlugs();
  }

  /**
   * Clear search
   */
  clearSearch() 
  {
      this.searchName    = '';
      this.sortFieldname     = 'id';
      this.sortOrder         = 'desc';
      this.pagination.offset = 0;
      this.getSlugs();
  }

}
