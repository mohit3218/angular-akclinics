// Imports
import { NgModule } from '@angular/core';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { MetaGuard } from 'ng2-meta';

import { Theme } from './Theme/Theme';
import { AuthTheme } from './Auth/Theme/AuthTheme'

import { Login } from './Auth/Login/Login';
import { PageSummary } from './Components/Page/PageSummary';
import { PageForm } from './Components/Page/PageForm';
import {AuthGuard} from './Services/auth-guard'
import { FrontendTheme } from './FrontendTheme/FrontendTheme';
import { Home } from './Components/Home/Home';
import { ContactSummary } from './Components/Contact/ContactSummary';
import { ContactFrontend } from './Components/ContactFrontend/ContactFrontend';
import { SlugSummary } from './Components/Slug/SlugSummary';
import { SlugForm } from './Components/Slug/SlugForm';
// Routes
const routes: Routes =
    [
        {
            path: 'auth',
            component: AuthTheme,
            children:
                [
                    //Login
                    { path: 'login', component: Login, pathMatch: 'full',  },
                ]
        },
       
        {
            path: '',
            component: FrontendTheme,
            children:
                [
                    { path: '', component: Home, pathMatch: 'full', canActivate: [MetaGuard], data: {
                        meta: {
                          title: 'Home page'
                        }
                      }  },
                    { path: 'contact', component: ContactFrontend, pathMatch: 'full' },
                ],
        },
        //Site routes goes here 
        {
            path: 'admin',
            component: Theme,
            children:
                [
                    // Slugs
                    { path: 'slugs', component: SlugSummary, pathMatch: 'full' },
                    { path: 'slugs/add', component: SlugForm, pathMatch: 'full' },
                    { path: 'slugs/edit/:id', component: SlugForm, pathMatch: 'full' },
                    // Pages
                    { path: 'pages', component: PageSummary, pathMatch: 'full' },
                    { path: 'pages/add', component: PageForm, pathMatch: 'full' },
                    { path: 'pages/edit/:id', component: PageForm, pathMatch: 'full' },
                     // Contacts
                     { path: 'contacts', component: ContactSummary, pathMatch: 'full' },
                ]
        }
    ];

// Decorators
@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

// App Routing
export class AppRoutingModule { }
