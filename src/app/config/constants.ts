/**
* @Author  : Mohit Kumar
* @Created : 24/09/2018
*/
export class Constants
{
    SiteName = 'AK Clinics';
    AdminRole = 1;
    ApiUrl = 'http://dev.laravel-akclinics.com/api';
    //ApiUrl = 'http://akclinics.co.in/laravel-akclinics/public/api';

    ENQUIRE_TYPES = [
        { id: 1, name: "Hair Transplant" },
        { id: 2, name: "Cosmetic Surgery" },
        { id: 3, name: "Cosmetology" },
    ]

    mainMenu = [
        {id : 1, name : 'HOME', href :''},
        {id : 2, name : 'About Us', href :'about-us'},
        {id : 3, name : 'Hair Transplant', href :'hair-transplat'},
        {id : 4, name : 'Hair Loss', href :'hair-loss'},
        {id : 5, name : 'Hair Restoration', href :'hair-restoration'},
        {id : 6, name : 'Cosmetic Surgery', href :'cosmetic-surgery'},
        {id : 7, name : 'Cosmetology', href :'cosmetology'},
        {id : 8, name : 'Results', href :'results'},
        {id : 9, name : 'Blog', href :'blogs'},
        {id : 10, name : 'Our Locations', href :'our-locations'},
        {id : 11, name : 'Contact Us', href :'contact-us'},
    ]


    // Page ID's
    homePageId = 1; //Home Page id
}