/**
 * @Author  : Mohit Kumar
 * @Created : 24/09/2018
 */
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject, ReplaySubject, from, of, range } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { User } from '../Models/User';
import { Constants } from '../config/constants';
import { JwtHelperService } from '@auth0/angular-jwt';
import { CookieService } from 'ngx-cookie-service';
// import { JwtHelperService } from '@auth0/angular-jwt';

// constants
const constants = new Constants;
const helper = new JwtHelperService();
// Decorators
@Injectable()

/**
 * Auth Class For Handling Login Process
 */
export class Auth 
{

    

    private usersUrl = `${constants.ApiUrl}/users/login`;  // URL to web api

    // Constructor
    constructor(private http: HttpClient, private cookieService: CookieService) { }

    /**
     * @param user
     *  POST: Login  
     */
    login(user: User): Observable<User>
    {
        return this.http.post<User>(this.usersUrl, user).pipe(
            tap((user: User) => this.log(`Login w/ id=${user.email}`)),
            catchError(this.handleError<User>('loginUser'))
        );
    }
    /**
     * return true if token is not expired
     */
    public isAuthenticated(): boolean {
        const token = this.cookieService.get('token');
        // Check whether the token is expired and return
        // true or false
        return !helper.isTokenExpired(token);
      }
    /**
    * Handle Http operation that failed.
    * Let the app continue.
    * @param operation - name of the operation that failed
    * @param result - optional value to return as the observable result
    */
    private handleError<T>(operation = 'operation', result?: T)
    {
        return (error: any): Observable<T> => 
        {

            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            this.log(`${operation} failed: ${error.message}`);

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }

    /** Log a HeroService message with the MessageService */
    private log(message: string)
    {
        console.log(message);
    }

}
