import { Injectable } from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {Auth} from './Auth';

@Injectable()
export class AuthGuard  implements CanActivate {

  constructor(private auth : Auth, private router : Router)
	{
  }
  
  /**
   * Check which route can be publically accessed
   */
  canActivate(): boolean {
    if (!this.auth.isAuthenticated()) {
      this.router.navigate(['/auth/login']);
      return false;
    }
    return true;
  }
}
