// Imports
import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute, RouterLink } from '@angular/router';
import { LoadingBarService } from '@ngx-loading-bar/core';
import { NgxSpinnerService } from 'ngx-spinner';
// import { Layout } from '../assets/layouts/layout/scripts/layout.js';
import { JwtHelperService } from '@auth0/angular-jwt';
import { CookieService } from 'ngx-cookie-service';
import { Constants } from './config/constants';
import { MetaService } from 'ng2-meta';

const cookieService = new CookieService(document);
const token = cookieService.get('token');
const helper = new JwtHelperService();
const constants = new Constants();



// Decorators
@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
})

// App Component
export class AppComponent implements OnInit
{
    name :RouterLink;
    // Declarations
    urlPath: string;
    route:ActivatedRoute;
    prefix = [];
    // Constructor
    constructor(public loader: LoadingBarService, public router: Router, public spinner: NgxSpinnerService, private metaService: MetaService)
    {

    }

    ngOnInit()
    {
        setTimeout(() => {
            /** spinner ends after 5 seconds */
            this.spinner.hide();
        }, 500);

        this.router.events.subscribe((e) =>
        {
            if (e instanceof NavigationEnd)
            {
                this.urlPath = e.url;
                this.prefix = this.urlPath.split('/');
                if(this.prefix[1] =='admin')
               {
                    if (token)
                    {
                        const isExpired = helper.isTokenExpired(token);
            
                        if (isExpired)
                        {
                            this.router.navigate(['/auth/login']);
                        }
                        // statement(s) will execute if the boolean expression is true
                    } 
                    else
                    {
                        this.router.navigate(['/auth/login']);
                        // statement(s) will execute if the boolean expression is false  
                    }
               }
              
            }
        });
    }

    // Updates sidebar and content height
    updateSidebarContentHeight()
    {
    }
}
