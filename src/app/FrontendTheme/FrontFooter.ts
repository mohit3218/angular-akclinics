/**
 * @Author  : Mohit Thakur
 * @Created : 12/06/2018
 */
import { Component, OnInit, AfterViewInit, AfterViewChecked, AfterContentChecked } from '@angular/core';
import { Router } from '@angular/router';
import { Common } from '../Services/Common';
import { Set } from '../Models/Set';
import { Constants } from '../config/constants';

const constants = new Constants
// Decorator
@Component({
    selector: 'front-footer',
    templateUrl: './footer.html',
})

/**
 * FrontFooter class
 */
export class FrontFooter implements OnInit 
{
    searchArray;
    searchName  = '';
    sortFieldname   = 'id';
    sortOrder       = 'desc';
    pageToNavigate = 1;
    Settings  = [];

    // constructor
    constructor(public router: Router,
        private commonService : Common) { }

    ngOnInit()
    {
    }

}
