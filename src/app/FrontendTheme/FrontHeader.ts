/**
 * @Author  : Mohit Thakur
 * @Created : 12/06/2018
 */
import { Component, OnInit } from '@angular/core';
// import { constants } from 'fs';
import { Constants } from '../config/constants';
import {EncodeURI} from './url';
import { Common } from '../Services/Common';


const constants = new Constants;
// Decorator
@Component({
    selector: 'front-header',
    templateUrl: './header.html',
})

/**
 * FrontHeader class
 */
export class FrontHeader implements OnInit
{
   
    searchArray ;
    searchName;
    sortFieldname   = 'id';
    sortOrder       = 'desc';
    pageToNavigate = 1;
    Settings  = [];

    ngOnInit()
    {
    }
    // constructor
    constructor(private commonService : Common) { }

}
