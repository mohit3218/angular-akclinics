/**
 * @Author  : Mohit Thakur
 * @Created : 12/06/2018
 */

import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-frontend-theme',
  templateUrl: './frontend-theme.html',
  styleUrls: ['./frontend-theme.css']
})
export class FrontendTheme implements OnInit {

  constructor( public spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.spinner.show();
    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.spinner.hide();
  }, 2000);
  }

  getState(outlet) {
    return outlet.activatedRouteData.state;
  }

}
