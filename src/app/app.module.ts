// Modules
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { CookieService } from 'ngx-cookie-service';
import { FlashMessagesModule } from 'angular2-flash-messages';
import { JwtModule } from '@auth0/angular-jwt';
// import { BsDatepickerModule } from 'ngx-bootstrap';
import { AppRoutingModule } from './/app-routing.module';
import { ModalModule } from 'ngx-bootstrap';
import { BsDropdownModule } from 'ngx-bootstrap';
import {ToastaModule} from 'ngx-toasta';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgxSpinnerModule } from 'ngx-spinner';
import { MetaModule } from 'ng2-meta';

// Components
import { AppComponent } from './app.component';
import { Theme } from './Theme/Theme';
import { Header } from './Theme/Header';
import { Logo } from './Theme/Logo';
import { Sidebar } from './Theme/Sidebar';
import { Footer } from './Theme/Footer';
import { Login } from './Auth/Login/Login'
import { AuthTheme } from './Auth/Theme/AuthTheme';
import { PageSummary } from './Components/Page/PageSummary';
import { PageForm } from './Components/Page/PageForm';
import { Paginate } from './Components/Paginate/Paginate';
import { ContactSummary } from './Components/Contact/ContactSummary';
import { ContactFrontend } from './Components/ContactFrontend/ContactFrontend';
import { SlugSummary } from './Components/Slug/SlugSummary';
import { SlugForm } from './Components/Slug/SlugForm';

// Services
import { Auth } from './Services/Auth';
import { Common } from './Services/Common';

// Directives
import { SortingDirective } from './sorting.directive';
// Pipes
import { DatePipe } from '@angular/common';
import { AuthGuard } from './Services/auth-guard';
import { FrontendTheme } from './FrontendTheme/FrontendTheme';
import { FrontHeader } from './FrontendTheme/FrontHeader';
import { FrontFooter } from './FrontendTheme/FrontFooter';
import { Home } from './Components/Home/Home';
import {ContactForm} from './Components/ContactFrontend/Contact/ContactForm';
import {EncodeURI} from './FrontendTheme/url';

@NgModule({

    declarations: [
        AppComponent,
        Theme,
        Header,
        Logo,
        Sidebar,
        Footer,
        Paginate,
        SortingDirective,
        Login,
        AuthTheme,
        PageSummary,
        PageForm,
        Paginate,
        FrontendTheme,
        FrontHeader,
        FrontFooter,
        Home,
        ContactSummary,
        ContactFrontend,
        ContactForm,
        SlugSummary,
        SlugForm,
        EncodeURI
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,
        AppRoutingModule,
        LoadingBarHttpClientModule,
        FlashMessagesModule.forRoot(),
        BrowserAnimationsModule,
        NgxSpinnerModule,
        // BsDatepickerModule.forRoot(),
        ToastaModule.forRoot(),
        BrowserAnimationsModule,
        ModalModule.forRoot(),
        BsDropdownModule.forRoot(),
        MetaModule.forRoot()
    ],
    providers: [Auth, Common,
        CookieService, DatePipe, AuthGuard],
    bootstrap: [AppComponent]
})
export class AppModule { }