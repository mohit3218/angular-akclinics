/**
 * @Author  : Mohit Kumar
 * @Created : 24/09/2018
 */

import { Component } from '@angular/core';

// Decorators
@Component(
    {
        selector: 'app-theme',
        templateUrl: './theme.html',
        styleUrls: ['./theme.css']
    })

/**
 * Theme Class
 */
export class Theme { }  
