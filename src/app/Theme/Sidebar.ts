/**
 * @Author  : Mohit Kumar
 * @Created : 24/09/2018
 */
import { Component } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Constants } from '../config/constants';

const cookieService = new CookieService(document);
const constants = new Constants;

// Decorators
@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.html',
})

/**
 * Sidebar Class
 */
export class Sidebar 
{
    role = Number(cookieService.set('role', '1'));
    constant = constants;
}
