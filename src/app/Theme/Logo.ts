/**
 * @Author  : Mohit Kumar
 * @Created : 24/09/2018
 */
import { Component } from '@angular/core';

// Decorators
@Component({
    selector: 'app-logo',
    templateUrl: './logo.html',
})

/**
 * Logo Class
 */
export class Logo { }
