/**
 * @Author  : Mohit Kumar
 * @Created : 24/09/2018
 */
import { Component } from '@angular/core';

// Decorators
@Component({
    selector: 'app-footer',
    templateUrl: './footer.html',
})

/**
 * Footer Class
 */
export class Footer { }
