export class Set
{
    id: number;
    tag_line: string;
    contact_first: string;
    contact_second: string;
    working_hours: string;
    address: any;
    email: any;
    description: number;
    facebook: string;
    twitter: any;
    linkedin: string;
    pinterest: string;
    rss: string;
}
