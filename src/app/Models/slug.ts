/**
 * @Author  : Mohit Thakur
 * @Created : 29/09/2018
 */
export class Slug
{
    id: number;
    name: string;
    is_active: number;
    created_by: number;
    created_at: string;
    updated_by: number;
    updated_at: string;
}
