// Model Class
export class Pagination {

    // Declrations
    total: number; //Total Records
    current_page: number;
    total_pages: number;
    per_page: number;
    from: number;
    to: number;
    number_of_links: number;
    pagination_links = [];
    limit: 5;
    offset: 0;
  
  // Updates pagination model
    update(obj) {
      this.total = obj.total;
      this.current_page = obj.current_page;
      this.per_page = obj.per_page;
      this.from = obj.from;
      this.to = obj.to;
      this.number_of_links = 5; // How many page numbers need to show 
      this.total_pages = Math.ceil(this.total / this.per_page);
      this.updatePaginationSet();
    }
  
    // Updates the pagination set - Start and End page numbers
    updatePaginationSet() {
      let start, end;
  
      // Makes the start number
      if (this.current_page < (this.number_of_links / 2) )
        start = 1;
      else
        start = this.current_page - Math.floor(this.number_of_links / 2);
  
      // Makes the end number
      if ( (start + this.number_of_links) > this.total_pages )
      {
        end = this.total_pages;
        start = end - this.number_of_links > 0 ? end - this.number_of_links + 1 : 1;
      }
      else
      {
        end = start + this.number_of_links - 1;
      }
  
      this.pagination_links = [];
  
      //Pushes the page numbers in pagination links array
      for (let i = start; i <= end; i++) {
        this.pagination_links.push(i);
      }
      //console.log(start); console.log(end); console.log(this.pagination_links);
    }
  }
  