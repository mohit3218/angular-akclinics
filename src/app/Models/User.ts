/**
 * @Author  : Mohit Kumar
 * @Created : 24/09/2018
 */
export class User
{
    id: number;
    role: number;
    email: string;
    firstname: string;
    lastname: string;
    password: string;
    confirmpassword: string;
    newpassword: string;
    is_active: number;
    api_token: string;
    token: string;
    created_by: number;
    created_at: string;
    updated_by: number;
    updated_at: string;
    oldpassword: any;
}
