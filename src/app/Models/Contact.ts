/**
 * @Author  : Mohit Thakur
 * @Created : 27/09/2018
 */
export class Contact
{
    id: number;
    name: string;
    email: string;
    enquire_types: number;
    phone: number;
    subject: any;
    message: any;
    city: string;
    country: string;
    created_by: number;
    created_at: string;
    updated_by: number;
    updated_at: string;
}
