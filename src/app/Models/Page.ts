/**
 * @Author  : Mohit Kumar
 * @Created : 24/09/2018
 */
export class Page
{
    id: number;
    name : string;
    slug_id : any = '';
    meta_title : string;
    meta_keywords : string;
    meta_description : string;
    og_title : string;
    og_description : string;
    og_image : string;
    is_active: number;
    created_by: number;
    created_at: string;
    updated_by: number;
    updated_at: string;
    deleted_at: string;
}
